import mongoose from 'mongoose'
import bodyParser from 'body-parser'// to get the access to post we use bodyParser
import express from 'express'
import cors from 'cors'// Cors allows us to access the  resources on a web page like images, videos etc.
import UserModel from './UserModel.js'// connecting to the UserModel 

mongoose.connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo',
 { useNewUrlParser: true , useUnifiedTopology: true, retryWrites: false },
 ()=>console.log("connected"), (e)=> console.log(e) )// connecting to the DB

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())//


app.get('/', (req, res) => {
  res.send('Just a test')
})// this route is used to test the connectivity

app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
}) // when we try to GET the information of users using find() in the UserModel

app.get('/users/:userID', async (req, res) => {// Using async as it always returns the value where as await waits for a value
  const user = await UserModel.findById(req.params.userID)// Using FindById to get the information of the user by ID
  if (user)
      return res.send(user)
  res.status(404).send(" User Not found")//Setting the status to send us message user not found when the ID is not found
})

app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) {
      console.log(err)
      res.send(err)
    }
    else res.send(newUser)
  })
})

app.put("/users/:userID", async (req, res)=>{// Using PUT operation to edit the Users information 
try{
  delete req.body._id;
  console.log(req.body, req.params.userID)
  let user =  await UserModel.findByIdAndUpdate(// Using findByIdANdUpdate to find the user by ID and update the informaton of the user
      req.params.userID, 
              req.body
      );
  if (user)
      res.send(user);
  else
      res.status(404).send("User ID Not found " +req.params.userID); // If ID is not found it 
    }
    catch(error){
      console.log(error)
      res.status(500).send(error) // if we there are errors tyhen we can get the error listed
    }
});

app.delete("/users/:userID", async (req, res)=>{
  
  let result = await  UserModel.findByIdAndDelete(req.params.userID);//we can delete a user by his ID
  if (result)
      res.send(result);
  else
      res.status(404).send("User ID not found");// If theID is not found the we get mesage User ID not found
});


app.listen(8080, () => console.log('Example app listening on port 8080!'))
